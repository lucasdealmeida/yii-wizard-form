<?php

use app\exceptions\PaymentException;
use app\services\PaymentServices;
use Codeception\Stub;

class FunctionalPaymentCest
{
    public function _before(\FunctionalTester $I)
    {
        $I->amOnPage(['site/step-one']);
    }

    public function submitFormSuccessfully(\FunctionalTester $I)
    {
        $paymentService = Stub::make(PaymentServices::class, ['make' => 'payment id']);
        Yii::$container->set(PaymentServices::class, $paymentService);

        $I->submitForm('#personal_information', [
            'first_name' => 'john',
            'last_name' => 'doe',
            'telephone' => '111-222',
        ]);

        $I->submitForm('#address_information', [
            'address' => 'main st.',
            'number' => '111',
            'city' => 'Murray',
            'zipcode' => '84109',
        ]);

        $I->submitForm('#payment_information', [
            'account_owner' => 'john doe',
            'iban' => '111222333',
        ]);

        $I->seeRecord('app\models\Customer', [
            'first_name' => 'john',
            'last_name' => 'doe',
            'telephone' => '111-222',
            'address' => 'main st.',
            'number' => '111',
            'city' => 'Murray',
            'zipcode' => '84109',
            'account_owner' => 'john doe',
            'iban' => '111222333',
            'payment_id' => 'payment id'
        ]);

        $I->see('Payment Successful!');
        $I->see('# payment id');
    }

    public function submitFormWithError(\FunctionalTester $I)
    {
        $paymentService = Stub::make(PaymentServices::class, ['make' => '']);
        $paymentService->method('make')->willThrowException(new PaymentException('Some payment error'));
        Yii::$container->set(PaymentServices::class, $paymentService);

        $I->submitForm('#personal_information', [
            'first_name' => 'john',
            'last_name' => 'doe',
            'telephone' => '111-222',
        ]);

        $I->submitForm('#address_information', [
            'address' => 'main st.',
            'number' => '111',
            'city' => 'Murray',
            'zipcode' => '84109',
        ]);

        $I->submitForm('#payment_information', [
            'account_owner' => 'john doe',
            'iban' => '111222333',
        ]);

        $I->cantSeeRecord('app\models\Customer', [
            'first_name' => 'john',
            'last_name' => 'doe',
            'telephone' => '111-222',
            'address' => 'main st.',
            'number' => '111',
            'city' => 'Murray',
            'zipcode' => '84109',
            'account_owner' => 'john doe',
            'iban' => '111222333',
        ]);
    }
}