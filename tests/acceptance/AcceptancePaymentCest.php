<?php

use yii\helpers\Url;

class AcceptancePaymentCest
{
    public function _before(\AcceptanceTester $I)
    {
        $I->amOnPage(Url::toRoute('/site/step-one'));
    }

    public function PaymentWorks(AcceptanceTester $I)
    {
        $I->see('Step 1', 'h4');
        $I->seeElement('#first_name');
        $I->fillField('first_name', 'John');
        $I->fillField('last_name', 'Doe');
        $I->fillField('telephone', '333-444');
        $I->click('Next');

        $I->seeElement('#address');

        $I->fillField('address', 'Main St.');
        $I->fillField('number', '123');
        $I->fillField('city', 'Murray');
        $I->fillField('zipcode', '81420');
        $I->click('Next');

        $I->seeElement('#account_owner');
        $I->fillField('account_owner', 'John Doe');
        $I->fillField('iban', '123456');
        $I->click('Next');

        $I->see('Payment successful!');
    }

    public function PaymentError(AcceptanceTester $I)
    {
        $I->see('Step 1', 'h4');
        $I->seeElement('#first_name');
        $I->fillField('first_name', 'John');
        $I->fillField('last_name', 'Doe');
        $I->fillField('telephone', '333-444');
        $I->click('Next');

        $I->seeElement('#address');

        $I->fillField('address', 'Main St.');
        $I->fillField('number', '123');
        $I->fillField('city', 'Murray');
        $I->fillField('zipcode', '81420');
        $I->click('Next');

        $I->seeElement('#account_owner');
        $I->click('Next');

        $I->see('Parameter Error');
    }
}
