<?php

namespace tests\unit\services;

use app\exceptions\PaymentException;
use app\models\ContactForm;
use app\services\PaymentServices;
use yii\httpclient\Client;
use yii\httpclient\Request;

class PaymentServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    public $tester;

    public function testPaymentSuccessful()
    {
        $request = $this->createMock(Request::class);
        $request->method('setMethod')->with('POST')->willReturnSelf();
        $request->method('setFormat')->with('json')->willReturnSelf();
        $request->method('setUrl')->willReturnSelf();
        $request->method('setData')
            ->with([
                'customerId' => 1,
                'iban' => 123,
                'owner' => 'John Doe',
            ])
            ->willReturnSelf();
        $request->method('send')->willReturn((Object) [
            'isOk' => true,
            'data' => [
                'paymentDataId' => 'payment id'
            ]
        ]);

        $httpClient = $this->createMock(Client::class);
        $httpClient->method('createRequest')->willReturn($request);

        $paymentService = new PaymentServices($httpClient);
        $result = $paymentService->make(1, 123, 'John Doe');

        expect($result)->equals('payment id');
    }

    public function testPaymentError()
    {
        $this->expectException(PaymentException::class);
        $this->expectExceptionMessage('Parameter error');

        $request = $this->createMock(Request::class);
        $request->method('setMethod')->willReturnSelf();
        $request->method('setFormat')->willReturnSelf();
        $request->method('setUrl')->willReturnSelf();
        $request->method('setData')->willReturnSelf();
        $request->method('send')->willReturn((Object) [
            'isOk' => false,
            'data' => [
                'error' => 'Parameter error',
            ],
        ]);

        $httpClient = $this->createMock(Client::class);
        $httpClient->method('createRequest')->willReturn($request);

        $paymentService = new PaymentServices($httpClient);
        $paymentService->make(1, 123, 'John Doe');
    }

    public function testPaymentUnexpectedError()
    {
        $this->expectException(PaymentException::class);
        $this->expectExceptionMessage('Unexpected error occurred.');

        $request = $this->createMock(Request::class);
        $request->method('setMethod')->willReturnSelf();
        $request->method('setFormat')->willReturnSelf();
        $request->method('setUrl')->willReturnSelf();
        $request->method('setData')->willReturnSelf();
        $request->method('send')->willReturn((Object) [
            'isOk' => false,
        ]);

        $httpClient = $this->createMock(Client::class);
        $httpClient->method('createRequest')->willReturn($request);

        $paymentService = new PaymentServices($httpClient);
        $paymentService->make(1, 123, 'John Doe');
    }
}
