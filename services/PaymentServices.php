<?php

namespace app\services;

use app\exceptions\PaymentException;
use yii\httpclient\Client;

class PaymentServices
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function make($customerId, $iban, $accountOwner)
    {
        $response = $this->client
            ->createRequest()
            ->setFormat(Client::FORMAT_JSON)
            ->setMethod('POST')
            ->setUrl('https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data')
            ->setData([
                'customerId' => $customerId,
                'iban' => $iban,
                'owner' => $accountOwner,
            ])

            ->send();

        if (!$response->isOk) {
            $message = 'Unexpected error occurred.';
            if (isset($response->data['error'])) {
                $message = $response->data['error'];
            }

            throw new PaymentException($message);
        }

        return $response->data['paymentDataId'];
    }
}