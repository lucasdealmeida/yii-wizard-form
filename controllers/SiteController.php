<?php

namespace app\controllers;

use app\exceptions\PaymentException;
use app\models\Customer;
use app\services\PaymentServices;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    protected $paymentService;

    public function __construct($id, $module, PaymentServices $paymentService, $config = [])
    {
        $this->paymentService = $paymentService;

        parent::__construct($id, $module, $config);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        if ($action->id === 'success') {
            return true;
        }

        if (
            Yii::$app->request->getReferrer() === null
            and Yii::$app->request->getReferrer() === NULL
            and in_array(Url::previous(), ['/', '/index.php', '/index-test.php']) === false
            and str_contains(Url::previous(), $action->id) === false
        ) {
            Yii::$app->response->redirect(Url::previous())->send();

            return false;
        }

        $session = Yii::$app->session;
        $request = Yii::$app->request;
        if (Yii::$app->request->getIsPost()) {
            $session->set('customer', array_merge(
                is_array($session->get('customer')) ? $session->get('customer'): [],
                $request->post()
            ));
        }

        Url::remember();

        return true;
    }

    public function actionStepOne()
    {
        if (Yii::$app->request->getIsPost()) {
            return $this->redirect(['site/step-two']);
        }

        return $this->render('step_one');
    }

    public function actionStepTwo()
    {
        if (Yii::$app->request->getIsPost()) {
            return $this->redirect(['site/step-three']);
        }

        return $this->render('step_two');
    }

    public function actionStepThree()
    {
        $request = Yii::$app->request;
        if ($request->getIsPost()) {
            $session = Yii::$app->session;

            $transaction = Customer::getDb()->beginTransaction();
            try {
                $customer             = new Customer();
                $customer->attributes = $session->get('customer');
                $customer->save();

                $customer->payment_id = $this->paymentService->make(
                    $customer->id,
                    $customer->iban,
                    $customer->account_owner
                );
                $customer->save();

                $transaction->commit();
                $session->destroy();
                return $this->redirect(['site/success', 'id' => $customer->id]);
            } catch(\Exception $e) {
                /**
                 * for security reasons only display the payment errors
                 * the internal errors should be sent for some error monitoring (NewRelic, Bugsnag, etc)
                 */
                $message = 'Internal error.';
                if ($e instanceof PaymentException) {
                    $message = $e->getMessage();
                }

                $transaction->rollBack();
                $session->setFlash('error', $message);
            }
        }

        return $this->render('step_three');
    }

    public function actionSuccess($id)
    {
        $customer = Customer::findOne($id);
        if ($customer === null) {
            throw new NotFoundHttpException();
        }

        return $this->render('success', [
            'customer' => $customer
        ]);
    }
}
