INSTALLATION
------------

### Install composer dependencies
~~~
docker-compose run --rm php composer install
~~~

### Start the docker containers
~~~
docker-compose up -d
~~~

### Run migrations
~~~
docker exec php yii migrate --interactive=0
~~~

### Access the application through the following URL:
~~~
http://127.0.0.1:8000
~~~

Tests
------------
### Run migrations
~~~
docker exec php tests/bin/yii migrate --interactive=0
~~~

### Run tests
~~~
docker exec php vendor/bin/codecept run
~~~