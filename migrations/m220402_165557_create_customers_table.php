<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%customers}}`.
 */
class m220402_165557_create_customers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%customers}}', [
            'id' => $this->primaryKey(),
            'first_name' => Schema::TYPE_STRING . ' NOT NULL',
            'last_name' => Schema::TYPE_STRING . ' NOT NULL',
            'telephone' => Schema::TYPE_STRING . ' NOT NULL',
            'address' => Schema::TYPE_STRING . ' NOT NULL',
            'number' => Schema::TYPE_STRING . ' NOT NULL',
            'city' => Schema::TYPE_STRING . ' NOT NULL',
            'zipcode' => Schema::TYPE_STRING . ' NOT NULL',
            'account_owner' => Schema::TYPE_STRING . ' NOT NULL',
            'iban' => Schema::TYPE_STRING . ' NOT NULL',
            'payment_id' => Schema::TYPE_STRING . ' NULL',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%customers}}');
    }
}
