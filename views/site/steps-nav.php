<?php
use yii\helpers\Html;

$steps = [
  ['heading' => 'Step 1', 'text' => 'Personal Information', 'route' => 'site/step-one'],
  ['heading' => 'Step 2', 'text' => 'Address Information', 'route' => 'site/step-two'],
  ['heading' => 'Step 3', 'text' => 'Payment', 'route' => 'site/step-three'],
];

?>
<ul class="mb-3 border rounded p-2 nav nav-pills nav-fill nav-justified">
    <?php foreach ($steps as $step): ?>
        <li class="nav-item">
            <?php
            $options = ['class' => 'nav-link'];

            if (Yii::$app->requestedRoute === $step['route']) {
                Html::addCssClass($options, 'active');
            }

            if (Yii::$app->requestedRoute === '' and $step['route'] === 'site/step-one') {
                Html::addCssClass($options, 'active');
            }

            echo Html::beginTag('div', $options)
            ?>
                <h4 class="list-group-item-heading"><?= $step['heading'] ?></h4>
                <p class="list-group-item-text"><?= $step['text'] ?></p>
            </div>
        </li>
    <?php endforeach; ?>
</ul>