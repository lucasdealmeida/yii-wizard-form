<?php
$this->title = 'Step 2 - Address Information';
?>
<div class="row">
    <div class="col-md-8 offset-md-2">
        <?= $this->render('steps-nav') ?>
        <div class="card">
            <div class="card-body">
                <form id="address_information" method="POST">

                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />

                    <div class="form-row">
                        <div class="form-group col-md-10">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" id="address" name="address">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="number">Number</label>
                            <input type="text" class="form-control" id="number" name="number">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-9 ">
                            <label for="city">City</label>
                            <input type="text" class="form-control" id="city" name="city">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="zipcode">Zip Code</label>
                            <input type="text" class="form-control" id="zipcode" name="zipcode">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Next</button>
                </form>
            </div>
        </div>
    </div>
</div>

