<?php
$this->title = 'Payment Successful!';
?>
<div class="row">
    <div class="col-md-8 offset-md-2">
        <div class="card">
            <div class="card-body">
                <h2>Payment Successful!</h2>
                <p># <?php echo $customer->payment_id; ?></p>
            </div>
        </div>
    </div>
</div>

