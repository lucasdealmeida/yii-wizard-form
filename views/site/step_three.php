<?php
$this->title = 'Step 3 - Payment';
?>
<div class="row">
    <div class="col-md-8 offset-md-2">
        <?= $this->render('steps-nav') ?>
        <div class="card">
            <div class="card-body">
                <form id="payment_information" method="POST">

                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="account_owner">Account Owner</label>
                            <input type="text" class="form-control" id="account_owner" name="account_owner">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="iban">IBAN</label>
                            <input type="text" class="form-control" id="iban" name="iban">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Next</button>
                </form>
            </div>
        </div>
    </div>
</div>

